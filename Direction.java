package testsnake;

// TODO: Auto-generated Javadoc
/**
 * The Class Direction.
 */
public class Direction {

	/** The Constant UP. */
	public static final int UP = 0;
	
	/** The Constant DOWN. */
	public static final int DOWN = 1;
	
	/** The Constant LEFT. */
	public static final int LEFT = 2;
	
	/** The Constant RIGHT. */
	public static final int RIGHT = 3;
	
}
