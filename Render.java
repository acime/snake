/*
 * 
 */
package testsnake;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * The class Render paints everything in the jframe.
 */
@SuppressWarnings("serial")
public class Render extends JPanel {

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		drawBackground(g);
		drawGrid(g);
		drawSnake(g);
		drawFood(g);
		drawScore(g);
		drawInstructions(g);

	}

	/**
	 * Draws the background.
	 *
	 * @param g
	 */
	public void drawBackground(Graphics g) {

		g.setColor(Color.black);
		g.fillRect(0, 0, Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);

	}

	/**
	 * Draws the grid.
	 *
	 * @param g
	 */
	public void drawGrid(Graphics g) {

		// Box, which contains lines.
		g.setColor(Color.white);
		g.drawRect(10, 10, Constants.GRID_SIZE * Constants.BOX_SIZE - Constants.BOX_SIZE,
				Constants.GRID_SIZE * Constants.BOX_SIZE - Constants.BOX_SIZE);

		// Vertical lines.
		for (int x = Constants.BOX_SIZE; x < Constants.GRID_SIZE * Constants.BOX_SIZE; x += Constants.BOX_SIZE) {
			g.drawLine(x, 10, x, Constants.BOX_SIZE * Constants.GRID_SIZE);
		}

		// Horizontal lines.
		for (int y = Constants.BOX_SIZE; y < Constants.GRID_SIZE * Constants.BOX_SIZE; y += Constants.BOX_SIZE) {
			g.drawLine(10, y, Constants.GRID_SIZE * Constants.BOX_SIZE, y);
		}

	}

	/**
	 * Draws the snake snake.
	 *
	 * @param g
	 */
	public void drawSnake(Graphics g) {

		g.setColor(Color.red);

		for (int i = 0; i < Snake.bodyParts.size(); i++) {
			g.fillRect(Snake.bodyParts.get(i).x * Constants.BOX_SIZE, Snake.bodyParts.get(i).y * Constants.BOX_SIZE,
					Constants.BOX_SIZE, Constants.BOX_SIZE);
		}

	}

	/**
	 * Draws the food.
	 *
	 * @param g
	 */
	public void drawFood(Graphics g) {

		g.setColor(Color.green);
		g.fillOval(Food.getFoodX() * Constants.BOX_SIZE, Food.getFoodY() * Constants.BOX_SIZE, Constants.BOX_SIZE,
				Constants.BOX_SIZE);

	}

	/**
	 * Draws the score.
	 *
	 * @param g
	 */
	public void drawScore(Graphics g) {

		g.setColor(Color.cyan);
		String foodEaten = "Food eaten: " + Snake.score;
		g.setFont(new Font("Trebuchet MS", 1, 20));
		g.drawString(foodEaten, 530, 40);

	}

	/**
	 * Draws the instructions.
	 *
	 * @param g
	 */
	public void drawInstructions(Graphics g) {

		if (Snake.isSnakeDead) {
			g.setColor(Color.ORANGE);
			String instructions = "Move the snake by pressing UP, DOWN, LEFT, RIGHT.";
			String instructions1 = "To start the game press DOWN, to restart press R.";
			g.setFont(new Font("Times New Roman", 1, 12));
			g.drawString(instructions, 530, 60);
			g.drawString(instructions1, 530, 80);
		}
	}

}
