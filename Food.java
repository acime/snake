package testsnake;

import java.awt.Point;
import java.util.Random;

/**
 * The class Food contains methods to operate with the food for snake.
 */
public class Food {

	/** The point of food. */
	static Point food = new Point();

	/**
	 * To get the x-coordinate of food.
	 *
	 * @return returns the x-coordinate of food
	 */
	public static int getFoodX() {

		return food.x;
	}

	/**
	 * To get the y-coordinate of food.
	 *
	 * @return returns the y-coordinate of food
	 */
	public static int getFoodY() {

		return food.y;
	}

	/**
	 * Generates a random position for food. If the food is generated on the
	 * snake, generates a new position for food.
	 *
	 * @return returns a point, which is the location of food.
	 */
	public static Point generateFoodPosition() {

		// Generates random position for the food.
		Random position = new Random();

		food.x = position.nextInt(Constants.GRID_SIZE - Constants.BOX_SIZE) + Constants.BOX_SIZE;
		food.y = position.nextInt(Constants.GRID_SIZE - Constants.BOX_SIZE) + Constants.BOX_SIZE;

		// Is the food on the snake ? If yes, generate a new position.
		for (Point point : Snake.bodyParts) {

			if (point.x == food.x && point.y == food.y) {
				food.x = position.nextInt(Constants.GRID_SIZE - Constants.BOX_SIZE) + Constants.BOX_SIZE;
				food.y = position.nextInt(Constants.GRID_SIZE - Constants.BOX_SIZE) + Constants.BOX_SIZE;
				return new Point(food.x, food.y);
			}

		}

		return new Point(food.x, food.y);

	}
}
