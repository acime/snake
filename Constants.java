package testsnake;

// TODO: Auto-generated Javadoc
/**
 * The Class Constants.
 */
public class Constants {

	/** The Constant BOX_SIZE - size of the box in the grid. */
	public static final int BOX_SIZE = 10;

	/** The Constant GRID_SIZE - amount of boxes in the grid. */
	public static final int GRID_SIZE = 50;

	/** The frame width. */
	public static final int FRAME_WIDTH = 850;

	/** The frame height. */
	public static final int FRAME_HEIGHT = 560;

	/** The Constant SNAKE_SPEED - interval between rendering. */
	public static final int SNAKE_SPEED = 50;

}
