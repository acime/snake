package testsnake;

import java.awt.Point;
import java.util.LinkedList;

/**
 * The class Snake contains variables and methods needed to operate with the
 * snake entity.
 */
public class Snake {

	/** The LinkedList contains Points, which are the bodyparts of the */
	static LinkedList<Point> bodyParts = new LinkedList<Point>();

	/** The amount of food the snake has eaten. */
	static int score = 0;

	/** The default direction the snake is moving to. */
	static int direction = Direction.DOWN;

	/** Boolean variable to know if the snake is dead or not. */
	static boolean isSnakeDead = true;

	/**
	 * Generates default position of the
	 */
	public static void generateSnake() {

		bodyParts.add(new Point(2, 2));
		bodyParts.add(new Point(2, 1));

	}

	/**
	 * Adds a new bodypart to the snake
	 */
	public static void growSnake() {

		bodyParts.add(bodyParts.size() - 1, new Point(Food.getFoodX(), Food.getFoodY()));
		score++;

	}

	/**
	 * Changes the position of bodyparts, depending on which direction the snake
	 * is going to.
	 */
	public static void moveSnake() {
		switch (direction) {

		case Direction.UP:
			bodyParts.remove(bodyParts.peekLast());
			bodyParts.add(0, new Point(bodyParts.peekFirst().x, bodyParts.peekFirst().y - 1));
			break;

		case Direction.DOWN:
			bodyParts.remove(bodyParts.peekLast());
			bodyParts.add(0, new Point(bodyParts.peekFirst().x, bodyParts.peekFirst().y + 1));
			break;

		case Direction.LEFT:
			bodyParts.remove(bodyParts.peekLast());
			bodyParts.add(0, new Point(bodyParts.peekFirst().x - 1, bodyParts.peekFirst().y));
			break;

		case Direction.RIGHT:
			bodyParts.remove(bodyParts.size() - 1);
			bodyParts.add(0, new Point(bodyParts.peekFirst().x + 1, bodyParts.peekFirst().y));
			break;
		}
	}

	/**
	 * Checks if the snake has collided into itself or has gone off the grid. If
	 * it has, it changes the variable isSnakeDead to true.
	 */
	@SuppressWarnings("unchecked")
	public static void collisionDetection() {

		// Has the snake collided into itself ?
		Point head = new Point(bodyParts.peekFirst().x, bodyParts.peekFirst().y);

		LinkedList<Point> snakeWithoutHead = new LinkedList<Point>();
		snakeWithoutHead = (LinkedList<Point>) bodyParts.clone();
		snakeWithoutHead.remove(0);

		if (snakeWithoutHead.contains(head)) {
			isSnakeDead = true;
			snakeWithoutHead.remove();
		} else {
			snakeWithoutHead.remove();
		}

		// Has the snake gone off the grid ?
		if (bodyParts.peekFirst().x >= Constants.GRID_SIZE || bodyParts.peekFirst().x < 1) {
			isSnakeDead = true;
		} else if (bodyParts.peekFirst().y >= Constants.GRID_SIZE || bodyParts.peekFirst().y < 1) {
			isSnakeDead = true;

		}
	}

	/**
	 * Checks if the head of snake has collided with food, if it has the snake
	 * grows bigger and new position is generated for the food.
	 */
	public static void foodAndGrow() {

		if (Food.getFoodX() == bodyParts.peekFirst().x && Food.getFoodY() == bodyParts.peekFirst().y) {
			Food.generateFoodPosition();
			growSnake();
		}

	}

}