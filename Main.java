package testsnake;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.Timer;

/**
 * The idea for the game (using the timer with ActionListener to render, the
 * render class) was taken from:
 * https://github.com/Jaryt23/SnakeTutorial/tree/master/snake
 * 
 * 
 * The class Main creates a GUI and handles key events.
 */
public class Main implements ActionListener, KeyListener {

	/** The jframe. */
	private JFrame jframe;

	/** The render. */
	private Render render = new Render();

	/** Constructs a timer. */
	private Timer timer = new Timer(Constants.SNAKE_SPEED, this);

	/**
	 * Instantiates a new main.
	 */
	public Main() {

		jframe = new JFrame("SNAKE");
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setVisible(true);
		jframe.add(render);
		jframe.addKeyListener(this);
		jframe.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
		jframe.setResizable(false);

	}

	/**
	 * The main method, creates a new Main, adds snake and food.
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		new Main();
		Snake.generateSnake();
		Food.generateFoodPosition();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		if (!Snake.isSnakeDead) {
			Snake.moveSnake();
			Snake.collisionDetection();
			Snake.foodAndGrow();
			render.repaint();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {

		case KeyEvent.VK_UP:
			if (Snake.direction == Direction.UP || Snake.direction == Direction.LEFT
					|| Snake.direction == Direction.RIGHT) {
				Snake.direction = Direction.UP;
			}
			break;

		case KeyEvent.VK_DOWN:
			if (Snake.direction == Direction.DOWN || Snake.direction == Direction.LEFT
					|| Snake.direction == Direction.RIGHT) {
				Snake.direction = Direction.DOWN;
				timer.start();

				if (Snake.isSnakeDead == true) {
					Snake.isSnakeDead = false;
				}
			}
			break;

		case KeyEvent.VK_LEFT:
			if (Snake.direction == Direction.LEFT || Snake.direction == Direction.UP
					|| Snake.direction == Direction.DOWN) {
				Snake.direction = Direction.LEFT;
			}
			break;

		case KeyEvent.VK_RIGHT:
			if (Snake.direction == Direction.RIGHT || Snake.direction == Direction.UP
					|| Snake.direction == Direction.DOWN) {
				Snake.direction = Direction.RIGHT;
			}
			break;

		case KeyEvent.VK_R:
			Snake.bodyParts.clear();
			Snake.generateSnake();
			Snake.direction = Direction.DOWN;
			timer.start();
			Snake.isSnakeDead = true;
			render.repaint();
			Snake.score = 0;
			timer.stop();
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent arg0) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent arg0) {

	}

}